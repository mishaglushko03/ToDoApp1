﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Context;
using ToDoApp.Domain.Models;

namespace ToDoApp.DataAccess.Repositories.Implementation
{
    public class GroupsRepository : BaseRepository<Groups, int, ToDoContext>, IGroupsRepository
    {
        public GroupsRepository(ToDoContext context) : base(context)
        {
        }
        public async Task<Groups>GetByNameAsync(char name)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Name == name);
        }
    }
}
