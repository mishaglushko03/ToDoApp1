﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Context;
using ToDoApp.Domain.Models;

namespace ToDoApp.DataAccess.Repositories.Implementation
{
    public class ExpHeaderRepository : BaseRepository<ExpenseHeader, int, ToDoContext>, IExpHeaderRepository
    {
        public ExpHeaderRepository(ToDoContext context) : base(context)
        {
        }

        public async Task<ExpenseHeader> GetByNameAsync(char description)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Description == description);
        }
    }
}
