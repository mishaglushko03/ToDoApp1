﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Context;
using ToDoApp.Domain.Models;

namespace ToDoApp.DataAccess.Repositories.Implementation
{
    public class UserRepository : BaseRepository<Users, int, ToDoContext>, IUsersRepository
    {
        public UserRepository(ToDoContext context) : base(context)
        {
        }

        public async Task<Users> GetByNameAsync(string name)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Name == name);
            
        }
    }
}
