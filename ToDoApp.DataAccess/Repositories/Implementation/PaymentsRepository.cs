﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Context;
using ToDoApp.Domain.Models;

namespace ToDoApp.DataAccess.Repositories.Implementation
{
    public class PaymentsRepository : BaseRepository<Payments, int, ToDoContext>, IPaymentsRepository
    {
        public PaymentsRepository(ToDoContext context) : base(context)
        {
        }

        public async Task<Payments> GetByNameAsync(char description)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Description == description);
        }
    }
}
