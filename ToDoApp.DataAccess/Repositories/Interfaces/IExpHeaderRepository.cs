﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.DataAccess.Repositories.Interfaces
{
    public interface IExpHeaderRepository : IRepository<ExpenseHeader, int>
    {
        Task<ExpenseHeader> GetByNameAsync(char description);
    }
}
