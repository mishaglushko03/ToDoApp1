﻿using Ardalis.Specification;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.DataAccess.Repositories.Interfaces
{
    public interface IUsersRepository : IRepository<Users, int>
    {
        Task<Users> GetByNameAsync(string name);
    }

}
