﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.DataAccess.Repositories.Interfaces
{
    public interface IPaymentsRepository : IRepository<Payments, int>
    {
        Task<Payments> GetByNameAsync(char description);

    }
}
