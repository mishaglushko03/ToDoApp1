﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.DataAccess.Repositories.Interfaces
{
    public interface IGroupsRepository : IRepository<Groups, int>
       
    {
        Task<Groups> GetByNameAsync(char name);
        Task<IEnumerable<Groups>> GetGroupsbyUserAsync(int user);
    }
}
