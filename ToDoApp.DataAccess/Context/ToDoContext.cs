﻿using Microsoft.EntityFrameworkCore;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Models;


namespace ToDoApp.Domain.Context
{
    public class ToDoContext : DbContext, IUnitOfWork
    {
        public DbSet<Users> Users { get; set; }
        public DbSet<Groups> Groups { get; set; }
        public DbSet<UserGroups> UserGroups { get; set; }
        public DbSet<Payments> Payments { get; set; }
        public DbSet<ExpenseHeader> ExpenseHeaders { get; set; }
        public DbSet<ExpenseLine> ExpenseLines { get; set; }

        public ToDoContext(DbContextOptions<ToDoContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Users>(b =>
            {
                b.HasKey(x => x.Id);
                b.Property(x => x.Name)
                    .HasMaxLength(30);
            });
            modelBuilder.Entity<Groups>(b =>
            {
                b.HasKey(x => x.Id);
                b.Property(x => x.Name)
                    .HasMaxLength(30);
            });
            modelBuilder.Entity<UserGroups>(b =>
            {
                b.HasOne(x => x.Users)
                    .WithMany(u => u.UserGroups)
                    .HasForeignKey(x => x.UserID);
                b.HasOne(x => x.Groups)
                    .WithMany(g => g.UserGroups)
                    .HasForeignKey(x => x.GroupID);
            });
            modelBuilder.Entity<Payments>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.Users)
                    .WithMany(u => u.Payments)
                    .HasForeignKey(x => x.FromUserId)
                    .OnDelete(DeleteBehavior.NoAction);
                b.HasOne(x => x.Groups)
                    .WithMany(g => g.Payments)
                    .HasForeignKey(x => x.GroupId)
                    .OnDelete(DeleteBehavior.NoAction);
                b.HasOne(x => x.ExpenseLine)
                    .WithMany(el => el.Payments)
                    .HasForeignKey(x => x.ToUserId)
                    .OnDelete(DeleteBehavior.NoAction);
                b.Property(x => x.Description)
                    .HasMaxLength(50);
            });
            modelBuilder.Entity<ExpenseHeader>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.Users)
                    .WithMany(u => u.ExpenseHeaders)
                    .HasForeignKey(x => x.UserId);
                b.HasOne(x => x.Groups)
                    .WithMany(g => g.ExpenseHeaders)
                    .HasForeignKey(x => x.GroupId);
                b.Property(x => x.Description)
                    .HasMaxLength(50);
                b.Property(x => x.Id).IsRequired();
            });
            modelBuilder.Entity<ExpenseLine>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.ExpenseHeader)
                    .WithMany(ex => ex.ExpenseLines)
                    .HasForeignKey(x => x.ExpenseID)
                    .OnDelete(DeleteBehavior.NoAction);
                b.HasOne(x => x.Users)
                    .WithMany(u => u.ExpenseLines)
                    .HasForeignKey(x => x.ForUserId)
                    .OnDelete(DeleteBehavior.NoAction);
            });
        }
    }

}
