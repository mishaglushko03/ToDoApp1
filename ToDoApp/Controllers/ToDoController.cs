﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Models;
using ToDoApp.Dto;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UsersApp.Controllers
{
    [Route("api/to-do")]
    [ApiController]
    public class ToDoController : ControllerBase
    {
        private readonly IUsersRepository _repository;

        public ToDoController(IUsersRepository repository)
        {
            _repository = repository;
        }
        // GET: api/<ToDoController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Users>>> Get()
        {
            var items = await _repository.GetAllAsync();
            return Ok(items.Select(x => new UsersDto
            {
                Id = x.Id,
                Name = x.Name
            }).ToList());
        }

        // GET api/<ToDoController>/5
        [HttpGet("{id}")]
        [Authorize()]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<ToDoController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] string name)
        {
            var item = new Users
            {
                Name = name
            };
            await _repository.InsertAsync(item);
            await _repository.UnitOfWork.SaveChangesAsync();

            return Ok();
        }

        // PUT api/<ToDoController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ToDoController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
