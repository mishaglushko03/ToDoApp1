﻿namespace ToDoApp.Dto
{
    public class UsersDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public object IsDone { get; internal set; }
    }
}
