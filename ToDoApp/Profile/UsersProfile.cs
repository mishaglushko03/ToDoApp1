﻿using AutoMapper;
using ToDoApp.Domain.Models;
using ToDoApp.Dto;

namespace ToDoApp.Profiles
{
    public class UsersProfile : Profile
    {
        public UsersProfile()
        {
            CreateMap<Users, UsersDto>()
                .ForMember(d => d.IsDone, opt => opt.MapFrom(s => s.IsCompleted));
        }
    }
}
