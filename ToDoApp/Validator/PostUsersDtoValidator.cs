﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApp.Dto;

namespace ToDoApp.Validators
{
    public class PostUsersDtoValidator : AbstractValidator<PostUsersDto>
    {
        public PostUsersDtoValidator()
        {
            RuleFor(x => x.Description)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .Length(1, 100);
        }
    }
}
