﻿using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using ToDoApp.BusinessLogic.Service.Implementation;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Models;
using Xunit;

namespace ToDoApp.BusinessLogic.Tests
{
    public class PaymentServiceTests
    {
            private readonly Mock<IUnitOfWork> _unitOfWorkMock;
            private readonly Mock<IPaymentsRepository> _repositoryMock;
            private readonly PaymentService _service;

            public PaymentServiceTests()
            {
                _unitOfWorkMock = new Mock<IUnitOfWork>();
                _unitOfWorkMock.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()))
                    .Returns(Task.FromResult(1));

                _repositoryMock = new Mock<IPaymentsRepository>();
                _repositoryMock.Setup(x => x.UnitOfWork)
                    .Returns(_unitOfWorkMock.Object);

                _service = new PaymentService(_repositoryMock.Object);
            }

            [Fact]
            public void GetUserPaymentGroup_ToDoItemDoesNotExist_ToDoItemNotFoundExceptionIsThrown()
            {
                //Arrange
                SetupGetPaymentsId(null);

                //Act
                //Assert
                Assert.ThrowsAsync<Exception>(async () => await _service.GetUserPaymentGroup(0,0));
            }

            [Theory]
            [InlineData(1, 2)]
            [InlineData(3, 4)]
            public void GetUserPaymentGroup_UserIsNotAnOwnerOfToDoItem_UserIsNotOwnerOfToDoItemExceptionIsThrown(int userIdPayment, int userId)
            {
                //Arrange
                var item = new Payments
                {
                    ToUserId = userIdPayment
                };
                SetupGetPaymentsId(item);

                //Act
                //Assert
                Assert.ThrowsAsync<Exception>(async () => await _service.GetUserPaymentGroup(userId, 0));
            }

            [Fact]
            public void CompleteToDoItemAsync_ToDoItemExistsAndUserIsOwner_UpdateAsyncIsCalled()
            {
                //Arrange
                int PaymentId = 2;
                int userId = 1;
                var item = new Payments
                {
                    Id = PaymentId,
                    ToUserId = userId
                };
                SetupGetPaymentsId(item);

                //Act
                _service.GetUserPaymentGroup(userId, PaymentId).Wait();

                //Assert
                _repositoryMock.Verify(x => x.UpdateAsync(
                    It.Is<Payments>(item => item.Id == PaymentId && item.IsCompleted)), Times.Once);
            }

            private void SetupGetPaymentsId(Payments payments)
            {
                _repositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                                .Returns(Task.FromResult(payments));
            }
    }
}
