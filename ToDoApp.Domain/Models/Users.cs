﻿using System.Collections.Generic;
using ToDoApp.Domain.Interfaces;

namespace ToDoApp.Domain.Models
{
    public class Users : IEntity<int>

    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<UserGroups> UserGroups { get; set; }
        public ICollection<Payments> Payments  { get; set; }
        public ICollection<ExpenseHeader> ExpenseHeaders  { get; set; }
        public ICollection<ExpenseLine> ExpenseLines  { get; set; }
        public object IsCompleted { get; set; }
    }
}
