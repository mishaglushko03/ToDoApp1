﻿using System.Collections.Generic;
using ToDoApp.Domain.Interfaces;

namespace ToDoApp.Domain.Models
{
    public class ExpenseLine : IEntity<int>
    {
        public int Id { get; set; }
        public int ExpenseID { get; set; }
        public int ForUserId { get; set; }
        public float Amount { get; set; }
        public Users Users { get; set; }
        public ExpenseHeader ExpenseHeader { get; set; }
        public ICollection<Payments> Payments { get; set; }
    }
}
