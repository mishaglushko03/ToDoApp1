﻿using System.ComponentModel.DataAnnotations;

namespace ToDoApp.Domain.Models
{
    public class UserGroups
    {
        [Key]
        public int UserID { get; set; }
        public int GroupID { get; set; }
        public Users Users { get; set; }
        public Groups Groups { get; set; }
    }
}
