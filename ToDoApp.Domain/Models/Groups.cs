﻿using System.Collections.Generic;
using ToDoApp.Domain.Interfaces;

namespace ToDoApp.Domain.Models
{
    public class Groups : IEntity<int>
    {
        public int Id { get; set; }
        public char Name { get; set; }
        public ICollection<UserGroups> UserGroups  { get; set; }
        public ICollection<Payments> Payments { get; set; }
        public ICollection<ExpenseHeader> ExpenseHeaders { get; set; }
        public IEnumerable<Groups> Users { get; set; }
    }
}
