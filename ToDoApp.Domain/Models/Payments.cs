﻿using System;
using ToDoApp.Domain.Interfaces;

namespace ToDoApp.Domain.Models
{
    public class Payments : IEntity<int>
    {
        public int Id { get; set; }
        public char Description { get; set; }
        public DateTime Time { get; set; }
        public float Amount { get; set; }
        public bool Confirmed { get; set; }
        public int FromUserId { get; set; }
        public int ToUserId { get; set; }
        public int GroupId { get; set; }
        public Users Users { get; set; }
        public Groups Groups { get; set; }
        public ExpenseLine ExpenseLine { get; set; }
        public bool IsCompleted { get; set; }
    }
}
