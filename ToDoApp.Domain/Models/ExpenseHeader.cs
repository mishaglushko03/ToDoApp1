﻿using System;
using System.Collections.Generic;
using ToDoApp.Domain.Interfaces;

namespace ToDoApp.Domain.Models
{
    public class ExpenseHeader : IEntity<int>
    {
        public int Id { get; set; }
        public char Description { get; set; }
        public DateTime Time { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public Users Users { get; set; }
        public Groups Groups { get; set; }
        public ICollection<ExpenseLine> ExpenseLines { get; set; }
}
}
