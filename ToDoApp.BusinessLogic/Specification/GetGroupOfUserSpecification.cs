﻿using Ardalis.Specification;
using System.Linq;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Services
{
    public class GetGroupOfUserSpecification : Specification<Users>
    {
        public GetGroupOfUserSpecification(int userId)
        {
            Query.Where(x => x.Id == userId)
                .Include(x => x.UserGroups);
        }
    }
}
