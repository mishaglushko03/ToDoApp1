﻿using Ardalis.Specification;
using ToDoApp.BusinessLogic.Helper;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Services
{
    public class GetUserConfirmPaymentGroupSpecification : Specification<Payments>
    {
        public GetUserConfirmPaymentGroupSpecification(int groupId, int userId)
        {
            Query.Where(x => SpecificationHelper.IsPaymentOfUser(x, userId, groupId)
                    && x.Confirmed);  
        }
    }
}
