﻿using Ardalis.Specification;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Services
{
    public class GetUserGroupSpecification : Specification<Groups>
    {
        public GetUserGroupSpecification(int grousId) 
        {
            Query.Where(x => x.Id == grousId)
                .Include(x => x.UserGroups);
        }
    }
}
