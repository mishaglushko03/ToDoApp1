﻿using Ardalis.Specification;
using System.Linq;
using ToDoApp.Domain.Models;


namespace ToDoApp.BusinessLogic.Services
{
    public class GetExpenseOfGroupSpecification : Specification<ExpenseHeader>
    {
        public GetExpenseOfGroupSpecification(int groupId)
        {
            Query.Where(x => x.GroupId == groupId)
                .Include(x => x.ExpenseLines);
        }
    }
}
