﻿using Ardalis.Specification;
using ToDoApp.BusinessLogic.Helper;
using ToDoApp.Domain.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace ToDoApp.BusinessLogic.Services
{
    public class GetUserPaymentSpecification : Specification<Payments>
    {
        public GetUserPaymentSpecification(int groupId, int userId) 
        {
            Query.Where(x => SpecificationHelper.IsPaymentOfUser(x, userId, groupId));
        }
    }
}
