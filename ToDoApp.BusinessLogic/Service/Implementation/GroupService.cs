﻿using System;
using System.Collections.Generic;
using ToDoApp.BusinessLogic.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.BusinessLogic.Service.Interfaces;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Service.Implementation
{
    public class GroupService : IGroupService
    {
        private readonly IGroupsRepository _groupsRepository;

        public GroupService(IGroupsRepository groupsRepository)
        {
            _groupsRepository = groupsRepository;
        }

        public async Task<IEnumerable<Groups>> GetUsers(int groupId)
        {
            var specification = new GetUserGroupSpecification(groupId);
            var group = await _groupsRepository.GetSingleAsync(specification);

            return group.Users;
        }

    }
}
