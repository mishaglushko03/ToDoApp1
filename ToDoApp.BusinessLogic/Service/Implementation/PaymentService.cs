﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.BusinessLogic.Service.Interfaces;
using ToDoApp.BusinessLogic.Services;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Models;


namespace ToDoApp.BusinessLogic.Service.Implementation
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentsRepository _paymentsRepository;
        public PaymentService(IPaymentsRepository paymentsRepository)
        {
           _paymentsRepository = paymentsRepository;
        }

        public async Task<IEnumerable<Payments>> GetUserConfirmPaymentGroup(int groupId, int userId)
        {
            var specification = new GetUserConfirmPaymentGroupSpecification(groupId, userId);
            return await _paymentsRepository.GetManyAsync(specification);
        }

        public async Task<IEnumerable<Payments>> GetUserPaymentGroup(int groupId, int userId)
        {
            var specification = new GetUserPaymentSpecification(groupId, userId);
            return await _paymentsRepository.GetManyAsync(specification);
        }
        public async Task<IEnumerable<Payments>> CalculateDebtFromPayments(int groupId, int userId, int toUserId, Debt debt)
        {

            var paymentsOfGroupSpecification = new GetUserConfirmPaymentGroupSpecification(groupId, userId);
            var payments = await _paymentsRepository.GetManyAsync(paymentsOfGroupSpecification);

            foreach (var payment in payments)
            {
                if (payment.FromUserId == userId && payment.ToUserId == toUserId)
                {
                    debt.Amount += (decimal)payment.Amount;
                }

                if (payment.FromUserId == toUserId && payment.ToUserId == userId)
                {
                    debt.Amount -= (decimal)payment.Amount;
                }
            }
        }

    }
}
