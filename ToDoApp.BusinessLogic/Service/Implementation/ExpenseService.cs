﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApp.BusinessLogic.Service.Interfaces;
using ToDoApp.BusinessLogic.Services;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Models;
using Debt = ToDoApp.BusinessLogic.Service.Interfaces.Debt;

namespace ToDoApp.BusinessLogic.Service.Implementation
{
    public class ExpenseHeaderService : IExpenseService
    {
        private readonly IExpHeaderRepository _expHeaderRepository;
        
        public ExpenseHeaderService(IExpHeaderRepository expHeaderRepository)
        {
            _expHeaderRepository = expHeaderRepository;
        }
        public async Task<ExpenseHeader> AddSumm(int groupId, int userId)
        {
            var specification = new GetUserConfirmPaymentGroupSpecification(groupId, userId);
            return await _expHeaderRepository.GetSingleAsync((Ardalis.Specification.ISpecification<ExpenseHeader>)specification);
        }
        public async Task<IEnumerable<ExpenseHeader>> GetGroupExpense(int groupId)
        {
            var specification = new GetExpenseOfGroupSpecification(groupId);
            return await _expHeaderRepository.GetManyAsync(specification);
        }

        private async Task<IEnumerable<ExpenseHeader>> CalculateDebtFromExpenses(int groupId, int userId, int toUserId, Debt debt)
        {
            var expensesOfGroupSpecification = new GetExpenseOfGroupSpecification(groupId);
            var expenses = await _expHeaderRepository.GetManyAsync(expensesOfGroupSpecification);

            foreach (var expense in expenses)
            {
                if (expense.UserId == userId)
                {
                    var expenseLine = expense.ExpenseLines.FirstOrDefault(x => x.ForUserId == toUserId);
                    if (expenseLine != null)
                    {
                        debt.Amount += (decimal)expenseLine.Amount;
                    }
                }

                if (expense.UserId == toUserId)
                {
                    var expenseLine = expense.ExpenseLines.FirstOrDefault(x => x.ForUserId == userId);
                    if (expenseLine != null)
                    {
                        debt.Amount -= (decimal)expenseLine.Amount;
                    }
                }
            }
        }

    }
}

