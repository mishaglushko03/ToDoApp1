﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.BusinessLogic.Service.Interfaces;
using ToDoApp.BusinessLogic.Services;
using ToDoApp.DataAccess.Repositories.Interfaces;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Service.Implementation
{
    public class UserService : IUserService
    {
        private readonly IUsersRepository _usersRepository;

        public UserService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<IEnumerable<Groups>> GetUserGroups(int userId)
        {
            var specification = new GetGroupOfUserSpecification(userId);
            return (IEnumerable<Groups>)await _usersRepository.GetManyAsync(specification);
        }
        public Task<Users> AuthenticateUserAsync(string login, string password)
        {
            if (login == "John Doe")
            {
                return Task.FromResult(new Users
                {
                    Id = 1,
                    Name = login
                });
            }

            return Task.FromResult((Users)null);
        }
        public Task<Users> RegisterUserAsync(string login, string password)
        {
            throw new NotImplementedException();
        }
    }
}
