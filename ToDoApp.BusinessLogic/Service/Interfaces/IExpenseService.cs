﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Service.Interfaces
{
    public interface IExpenseService
    {
        Task<IEnumerable<ExpenseHeader>> GetGroupExpense(int group);
        Task<ExpenseHeader> AddSumm(int groupId, int userId);
        Task<IEnumerable<ExpenseHeader>> CalculateDebtFromExpenses(int groupId, int userId, int toUserId, Debt debt);
        
    }

    public class Debt
    {
        public int UserId { get; set; }
        public decimal Amount { get; set; }
    }
}
