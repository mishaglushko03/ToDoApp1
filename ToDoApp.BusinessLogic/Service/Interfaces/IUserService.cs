﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Service.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<Groups>> GetUserGroups(int userId);
        Task<Users> AuthenticateUserAsync(string login, string password);
        Task<Users> RegisterUserAsync(string login, string password);
    }
}
