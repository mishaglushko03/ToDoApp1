﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Service.Interfaces
{
    public interface IPaymentService
    {
        Task<IEnumerable<Payments>> GetUserConfirmPaymentGroup(int groupId,int userId);
        Task<IEnumerable<Payments>> GetUserPaymentGroup(int groupId, int userId);
        Task<IEnumerable<Payments>> CalculateDebtFromPayments(int groupId, int userId, int toUserId, Debt debt);

    }
}
