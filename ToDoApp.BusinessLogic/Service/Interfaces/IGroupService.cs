﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Service.Interfaces
{
    public interface IGroupService
    {
        Task<IEnumerable<Groups>> GetUsers(int groupId);

    }
}
