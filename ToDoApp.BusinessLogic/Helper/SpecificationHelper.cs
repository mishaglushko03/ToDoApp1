﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.Domain.Models;

namespace ToDoApp.BusinessLogic.Helper
{
    public static class SpecificationHelper
    {
        public static bool IsPaymentOfUser(Payments payments, int userId, int groupId) 
        {
            return payments.GroupId == groupId && (payments.FromUserId == userId || payments.ToUserId == userId);
        }
    }
}
